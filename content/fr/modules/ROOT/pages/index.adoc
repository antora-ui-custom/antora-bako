= Garage Bako
:date: 2020-07-09
:lang: fr
:page-lang: fr
:page-about: À propos
:page-team: Équipe 
:page-contact: Contact 
:description: Nous sommes vos meilleurs mécaniciens à Gbekè !
include::partial$locale/attributes.adoc[]

[#px-1]
== Soyez les bienvenus ! 

=== {description}

[#about]
== Qui sommes nous ?

[role="section"]
--
Nous sommes *vos* mécaniciens à Gbekè !

Prêt à vous secourir au besoin.

Contact: 07 49 49 94 14; 05 06 52 41 66
--

[#team]
== L'équipe !

[#contact]
== Retrouvez nous à Bouaké !

[role="section"]
--
* Nous sommes faciles à trouver.  Google peut vous diriger. {zwsp} +
  {zwsp} +

https://www.google.com/maps/dir/Current+Location/7.70879,-5.00619[Go to Bako's!,role="button"]

{empty} +
* Prenez la route vers Belleville, Nous sommes à Belleville 1 au carréfour de l'ancien commisariat.
--
